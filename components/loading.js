import { h } from 'preact';
import './loading.css'

export default function Loading() {
    return <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
} 