import { useState, useEffect, useRef } from 'preact/hooks'
import { h } from 'preact';
import register from 'preact-custom-element';
import { getOrganizationUserNames, getStyles, getUserComplete } from '../api';
// import './organization-usernames.css';
import 'preact-material-components/Dialog/style.css';
import Dialog from 'preact-material-components/Dialog';
import 'preact-material-components/Button/style.css';

import { STYLES_URL } from '../_CONSTANTS_';
import Loading from '../components/loading';

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
    },
};

const OrgUsernames = ({ orgId, apiKey, theme = 'default' }) => {
    const [data, setData] = useState();
    const [isValidApiKey, setIsValidApiKey] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const [modalIsOpen, setIsOpen] = useState(false);
    const [userDetails, setUserDetails] = useState();
    const dialogRef = useRef(null);

    useEffect(async function () {
        let cancelled = false;
        if (apiKey === '4c322955-386f-4fee-a4b0-581af8dbf089') {
            setIsValidApiKey(true);
            const data = await getOrganizationUserNames(orgId);
            if (!cancelled)
                setData(data);
        }

        const stylesString = await getStyles(theme)
        var newSS = document.createElement('style');
        newSS.innerText = stylesString;
        document.getElementsByTagName("head")[0].appendChild(newSS);
        setIsLoading(false);
        return () => {
            cancelled = true;
        }
    }, []);

    function closeModal() {
        setIsOpen(false);
    }

    async function viewUserDetails(userId) {
        setUserDetails(await getUserComplete(userId));
        dialogRef.current.MDComponent.show();
    }

    return <div class="organization-container">
        {
            isLoading
                ?
                <Loading />
                :
                <div>
                    <a class="display-1 title">Organization users</a>
                    {
                        isValidApiKey ?
                            <div>
                                <Dialog ref={dialogRef} class="contact-dialog">
                                    <Dialog.Body scrollable={false}>
                                        <section >
                                            <h2 class="contact-info">Contact Information</h2>
                                            <div class="named-section__content-wrapper">
                                                <span class="fullname">{userDetails?.firstName} {userDetails?.lastName}</span>
                                            </div>
                                        </section>
                                        <div>
                                            <div class="named-section__content-wrapper">
                                                <ul class="profile-general-contact-list">
                                                    <li class="profile-general-contact-list__item">
                                                        <div class="contact-info">
                                                            <span class="job-title">Job</span>
                                                            <div class="job-value">
                                                                <a>{userDetails?.job.description}</a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="contact-list-wrapper">
                                                        <div class="contact-info">
                                                            <span class="email">Email</span>
                                                            <div class="">
                                                                <a href="mailto:robert.schonberger@yale.edu">{userDetails?.email}</a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </Dialog.Body>
                                    <Dialog.Footer>
                                        <Dialog.FooterButton accept={true}>close</Dialog.FooterButton>
                                    </Dialog.Footer>
                                </Dialog>
                                <ul>
                                    {
                                        data && data.map(u => <li class="list-group-item">
                                            <img style="width: 100px;height: 100px;" src="https://cdn.pixabay.com/photo/2020/07/14/13/07/icon-5404125_1280.png" />
                                            <span class="userId"><b>{u.userId}</b></span> <span class="fullName">{u.firstName} {u.lastName}</span>
                                            <a class="view-details" onClick={() => viewUserDetails(u.userId)}>VIEW DETAILS</a>
                                        </li>
                                        )
                                    }
                                </ul>
                            </div>
                            :
                            <span class="invalid-api-key">Invalid api key.</span>
                    }
                </div>
        }
    </div>;
}

register(OrgUsernames, 'org-usernames', ['orgId', 'apiKey', 'theme'], { shadow: false })