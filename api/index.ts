import axios from 'axios';
import { STYLES_URL } from '../_CONSTANTS_';
const format = require('string-format')

export async function getOrganizationUserNames(orgId){
    const url = `https://prod.profile.yale.edu/Organizations/${orgId}/UserNames`;
    const data = await fetch(url);
    return data.json();
}

export async function getUserComplete(userId){    
    const url = `https://prod.profile.yale.edu/Users/${userId}/Complete`;
    const data = await fetch(url);
    return data.json();
}

export async function getStyles(theme = 'default'){
    const url = `${format(STYLES_URL,theme)}`;
    const data = await fetch(url, {
        mode: 'cors',
        headers: {
            'Content-Type': 'text/plain'
        }
    }).then(response => response.text());
    return data;
}